var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify-es').default;
var cleanCss = require('gulp-clean-css');

gulp.task('jsBundle',function(){
    gulp.src(['./public/js/lib/**/*.js'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(concat('bundle.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js/'))
});
gulp.task('cssBundle',function(){
	gulp.src(['./public/css/lib/**/*.css'])
	.pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
	.pipe(concat('bundle.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('./public/css/'))
});
gulp.task('default',function(){
    browserSync.init({
        server: "./public/",
        port:8080,
    });
    gulp.watch('./public/js/lib/*.js',['jsBundle']);
    gulp.watch('./public/css/lib/*.css',['cssBundle']);
    gulp.watch('./public/css/*.css',reload);
    gulp.watch('./public/js/*.js',reload);
    gulp.watch('./public/*.html',reload);
    gulp.watch('./public/img/**/*',reload);
});
