import Vue from 'vue'
import Router from 'vue-router'
import LandingPage from '@/components/LandingPage'
import PrintPage from '@/components/PrintPage'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: LandingPage,
    },
    {
      path: '/PrintPage',
      name: 'PrintPage',
      component: PrintPage,
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
